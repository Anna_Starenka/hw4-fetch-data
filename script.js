/// AJAX - це назва для набору технік розробки веб-інтерфейсів, що дозволяють робити динамічні запити до сервера без 
//видимого перезавантаження веб-сторінки: користувач не помічає, коли його браузер запитує дані


const filmUrl = "https://ajax.test-danit.com/api/swapi/films";

fetch(filmUrl)
  .then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Not found");
    }
  })
  .then((res) => {
    displayFilms(res);
    return res;
  })

  .catch((error) => console.error(error));

function displayFilms(films) {
  films.forEach(({ characters, episodeId, name, openingCrawl }) => {
    const grid = document.querySelector(".container");
    const div = document.createElement("div");
    div.className = "item";
    grid.appendChild(div);
    div.innerHTML = `
       <h2> Episode - ${episodeId}</h2>
       <h3> Name - ${name}</h3>
       <p> <span>Brief content</span> <br> ${openingCrawl}</p>
       <span> Characters</span>
       <ul class ="characters-list">  </ul>
       `;
    const charactersList = div.querySelector(".characters-list");
    Promise.all(
      characters.map((url) => fetch(url).then((response) => response.json()))
    ).then((characters) => {
      charactersList.innerHTML = characters
        .map((character) => `<li> ${character.name}</li>`)
        .join("");
    });
  });
}
